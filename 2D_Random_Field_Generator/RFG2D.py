import numpy as np
from numpy.random import normal
import scipy.io as sio

'''
This code generates a structured grid of stochastic field.

2D Gaussian Random Field Generation

Description: This code generates 2D Gausian and isotropic random fields
of given correlation length. The random fields are generated on a grid of
Nx, Ny size

Usage:

To use this code, you need to specify the field parameters in an input file
named as INPUTDATA.txt

The parameters specified in the inputdata file are:

Lx - Dimension of the domain in the x direction
Ly - Dimension of the domain in the y direction
correlation_length - correlation length of the field
Nx - number of grid points (discretization) in the x direction
Ny - number of grid points (discretization) in the y direction
margin_parameter - the default method creates periodic random field. To create
aperiodic random field a subdomain is cropped. This subdomain is specified using
a margin parameter which the margin length (gap between subdomain boundary and domain boundary)
in terms of the correlation length. So, a margin_parameter of 1 means that margin length is 1*correlation_length
Ns - Number of realizations to be generated


MAKE SURE INPUTDATA.txt file with required parameters is present in the same folder as this code before running the code

'''

def RFG2D(Lx, Ly, b, Nx, Ny):
    
    " Nx - correspond to x axis ~ NO. OF COLUMNS"
    " Ny - correspond to y axis ~ NO. OF ROWS"
    " Lx- domain size along x - dir"
    " Ly- domain size along y - dir"
    " b - isotropic correlation length for EXPONENTIAL KERNEL"
    
    u = np.ones((Ny,Nx))
    v = np.ones((Ny,Nx))
    z = u + 1j*v
    "creating flags for the filling algorithm"
    z = 9999.0*z
    pi = 3.141592654
    
    "Nequist  = 0,1 depending on existence of Nequist"
    "This helps to create indexes for the coef matrix"
    if(np.mod(Nx,2)==1):
        armlengthx = int((Nx-1)/2)
        Nequistx = 0
    
    else:
        armlengthx = int((Nx-2)/2)
        Nequistx =1
    
    if(np.mod(Ny,2)==1):
        armlengthy = int((Ny-1)/2)
        Nequisty = 0
    
    else:
        armlengthy = int((Ny-2)/2)
        Nequisty =1

    "dwx, dwy ~ grid units in the Fourier Space"
    dwx = 2.0*pi/Lx
    dwy = 2.0*pi/Ly
    
    coef =  np.zeros((Ny,Nx))
    "The filling algorithm (to create conjugate sym. matrix)"
    for r in range(1,(Ny+1)):
        for c in range(1,(Nx+1)):
           "indices of the conjugate values"
           rcomp = np.mod(Ny - r + 1, Ny) + 1
           ccomp = np.mod(Nx - c + 1, Nx) + 1
           
           i = int(r - (armlengthy + 1))
           j = int(c - (armlengthx + 1))
           
           " coordinate of the point in K-space"
           wy = i*dwy
           wx = j*dwx
           
           "modify i,j to calculate the position the values (wx,wy) in coef"
           "matrix (Ny,Nx). This is based on the periodicity assumption of the K-space"
           "i.e. (i,j) ~ (i+-Ny, j+-Nx)"
           if( i < 0 ):
               i = int(Ny + i)
               
           if( j < 0 ):
               j = int(Nx + j)             
                            
           i = i+1
           j = j+1
           
           "S is derived from spectral Power density S(wx,wy)"
           " This of type Matern-class kernel"
           "v = 1/2 - i.e. exponential"
           d = np.sqrt((wx**2) + (wy**2))
           S = (1/(2*pi*b))*((1/((1/b)**2 + d**2))**1.5)
           
           coef[int(i-1),int(j-1)] = np.sqrt(0.5*dwx*dwy*S)
           
           if(z[r-1,c-1] == 9999.0*(1+1j*1)):
               z[r-1,c-1] = normal(0.0,1.0) + 1j*(normal(0.0,1.0))
               z[rcomp-1, ccomp-1] = np.conj(z[r-1,c-1])
               
               if((r == rcomp) and (c == ccomp)):
                   "sqrt(2) is required to rescale the RV"
                   
                   z[r-1,c-1] = 0.5*(z[r-1,c-1] + np.conj(z[r-1,c-1]))*np.sqrt(2)                 
                      
    coef[0,0] = 0
    Mat = np.multiply(coef,z)
    
    Y = int(Nx*Ny)*np.fft.ifft2(Mat)
    
    return np.real(Y)
'''
def ElistAssign(ELIST, RF, xi, yi, dx, dy):
    " This code requires ELIST.npy and RF.npy files or Arrays"
    " The ELIST array has NxM no. of rows having element no. and integration point"
    "information"
    "RF array is an MxN array, (X,Y) x ~ column, y ~ row, having Random field"   
    "THIS CODE assigns the 2D grid field (RF) to 1D form of ELIST"
    "The output is EFIELD"
    "xi,yi are the co-ordinates of the left-bottom end of the domain"
    "dx and dy are the element lengths along x and y direction respectively"    
    " Note: use numpy.loadtxt('ELIST.txt', dtype = float) to load ELIST in python"
    
    EFIELD = np.zeros((np.size(ELIST,0),2))
    
    for i in range(0,np.size(ELIST,0)):
        
        EFIELD[i,0] = ELIST[i,0]
        
        x = ELIST[i,1]
        y = ELIST[i,2]
        
        c = int(round(((x-xi)/dx) + 0.5))
        r = int(round(((y-yi)/dy) + 0.5))
        r=(r-1)
        c=(c-1)
        
        EFIELD[i,1] = RF[r,c]
        
    return EFIELD
    
'''


"THE MAIN SCRIPT READS INPUT PARAMETERS FROM THE INPUT.txt file and creates MATLAB ARRAYS"
def main(): 

    #STEP-1: READ IN THE INPUT SPECIFICATIONS:
    inputfile = open('INPUTDATA.txt', 'r+')
    "Convert the input file to a list of strings"
    inputlist = list(inputfile)

    counter = 0
    for item  in inputlist:
        "Ignore the first line"
        if(counter > 0):
            inputstring = item
            "Remove all the whitespace"
            inputstring.strip()
            "Remove the newline character ~ \n"
            inputstring = inputstring.replace('\n','')
            "Partition the string about '=' "
            temp = inputstring.partition('=')
            
            "FILL THE INPUT DATA"

            " Here, m is the margin factor for the randon field domain. This is used to create an aperiodic random field"
            " Lx0, Ly0 are the domain lengths along x,y direction in FEM model"
            " b is the correlation length for the isotropic exponential model"
            "Nx0, Ny0 defines the number of elements along x, y directions respectively in a Nx x Ny grid model"
            "Ns ~ No. of simulations"
            if(temp[0].strip()  == 'Lx'):
                Lx0 = float(temp[2])
            elif(temp[0].strip() == 'Ly'):
                Ly0 = float(temp[2])
            elif(temp[0].strip() == 'margin_parameter'):
                m = float(temp[2])
            elif(temp[0].strip() == 'correlation_length'):
                b = float(temp[2])
            elif(temp[0].strip() == 'Nx'):
                Nx0 = int(temp[2])
            elif(temp[0].strip() == 'Ny'):
                Ny0 = int(temp[2])
            elif(temp[0].strip() == 'Ns'):
                Ns = int(temp[2])
        counter = counter + 1

    "NOE defines the no. of elements in the model"
    NOE = Nx0*Ny0

    "dx, dy are grid units ~ i.e. the size of the rectangular mesh element"
    dx = Lx0/Nx0
    dy = Ly0/Ny0

    "Lx, Ly are the domain lengths along x,y direction in Randon Field model"
    Lx = 0.0
    Ly = 0.0

    "xi,yi are the co-ordinates of the left-bottom end of the domain"
    xi = 0.0
    yi = 0.0 

    "No. of elements to be added to create boundary margins"
    adddx = np.ceil(m*b/dx)
    adddy = np.ceil(m*b/dy)

    Lx = Lx0 + 2*adddx*dx
    Ly = Ly0 + 2*adddy*dy

    "Nx, Ny are the modified number of elements in the random field model"

    Nx = int(Nx0 + int(2*adddx))
    Ny = int(Ny0 + int(2*adddy))

    for i in range(0,Ns):
        
        "Generate the random field"
        RFfull = RFG2D(Lx, Ly, b, Nx, Ny)

        "Cut out the required portion - to make it aperiodic"
        RF = RFfull[int(adddy):int(Ny0 + adddy), int(adddx):int(Nx0 + adddx)]

        RFGname = 'RFG' + str(i+1)
        sio.savemat(RFGname, mdict={RFGname: RF})

    
main()
        
        
        
        
        
       


        
        
        
        
  
        
        

        
            
            
            
            
        

        

        




            
            
            
            
        

        

        





