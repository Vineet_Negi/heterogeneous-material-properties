import numpy as np
from numpy.random import normal
import scipy.io as sio

'''
3D Gaussian Random Field Generation

Description: This code generates 3D Gausian and isotropic random fields
of given correlation length. The random fields are generated on a grid of
Nx, Ny, Nz size

Usage:

To use this code, you need to specify the field parameters in an input file
named as INPUTDATA.txt

The parameters specified in the inputdata file are:

Lx - Dimension of the domain in the x direction
Ly - Dimension of the domain in the y direction
Lz - Dimension of the domain in the z direction
correlation_length - correlation length of the field
Nx - number of grid points (discretization) in the x direction
Ny - number of grid points (discretization) in the y direction
Nz - number of grid points (discretization) in the z direction
margin_parameter - the default method creates periodic random field. To create
aperiodic random field a subdomain is cropped. This subdomain is specified using
a margin parameter which the margin length (gap between subdomain boundary and domain boundary)
in terms of the correlation length. So, a margin_parameter of 1 means that margin length is 1*correlation_length
Ns - Number of realizations to be generated


MAKE SURE INPUTDATA.txt file with required parameters is present in the same folder as this code before running the code
'''




def RFG3D(Lx, Ly, Lz, b, Nx, Ny, Nz):
    
    " Nx - correspond to x axis ~ indices "
    " Ny - correspond to y axis ~ indices"
    " Nz - correspond to z axis ~ indices"
    " Lx- domain size along x - dir"
    " Ly- domain size along y - dir"
    " Lz- domain size along z - dir"
    " b - isotropic correlation length for EXPONENTIAL KERNEL"
    
    u = np.ones((Nz,Ny,Nx))
    v = np.ones((Nz,Ny,Nx))
    z = u + 1j*v
    "creating flags for the filling algorithm"
    z = 9999.0*z
    pi = 3.141592654
    
    "Nequist  = 0,1 depending on existence of Nequist"
    "This helps to create indexes for the coef matrix"
    if(np.mod(Nx,2)==1):
        armlengthx = int((Nx-1)/2)
        Nequistx = 0
    
    else:
        armlengthx = int((Nx-2)/2)
        Nequistx =1
   
    if(np.mod(Ny,2)==1):
        armlengthy = int((Ny-1)/2)
        Nequisty = 0
    
    else:
        armlengthy = int((Ny-2)/2)
        Nequisty =1
        
    if(np.mod(Nz,2)==1):
        armlengthz = int((Nz-1)/2)
        Nequistz = 0
    
    else:
        armlengthz = int((Nz-2)/2)
        Nequistz =1

    "dwx, dwy, dwz ~ grid units in the Fourier Space"
    dwx = 2.0*pi/Lx
    dwy = 2.0*pi/Ly
    dwz = 2.0*pi/Lz
    
    coef =  np.zeros((Nz,Ny,Nx))
    "The filling algorithm (to create conjugate sym. matrix)"
    for rz in range(1,(Nz+1)):
        
        for ry in range(1,(Ny+1)):
            
            for rx in range(1,(Nx+1)):
                
            
                "indices of the conjugate values"
                rzcomp = np.mod(Nz - rz + 1, Nz) + 1
                rycomp = np.mod(Ny - ry + 1, Ny) + 1
                rxcomp = np.mod(Nx - rx + 1, Nx) + 1
           
                iz = int(rz - (armlengthz + 1))
                iy = int(ry - (armlengthy + 1))
                ix = int(rx - (armlengthx + 1))
           
                " coordinate of the point in K-space"
                wz = iz*dwz
                wy = iy*dwy
                wx = ix*dwx
           
                "modify iz,iy,ix to calculate the position the values (wx,wy,wz) in coef"
                "matrix (Nz,Ny,Nx). This is based on the periodicity assumption of the K-space"
                "i.e. (iz,iy,ix) ~ (iz+-Nz, iy+-Ny, ix+-Nx)"
                if( iz < 0 ):
                    iz = int(Nz + iz)
               
                if( iy < 0 ):
                    iy = int(Ny + iy)
               
                if( ix < 0 ):
                    ix = int(Nx + ix)
               
               
                iz = iz+1
                iy = iy+1
                ix = ix+1
           
                "S is derived from spectral Power density S(wx,wy,wz)"
                " This of type Matern-class kernel"
                "v = 1/2 - i.e. exponential"
                d = np.sqrt((wx**2) + (wy**2) + (wz**2))
               
               
                S = (1/(pi*pi*b))*((1/((1/b)**2 + d**2))**2.0)
           
                coef[int(iz-1),int(iy-1),int(ix-1)] = np.sqrt(0.5*dwx*dwy*dwz*S)
           
                if(z[rz-1,ry-1,rx-1] == 9999.0*(1+1j*1)):
                    z[rz-1,ry-1,rx-1] = normal(0.0,1.0) + 1j*(normal(0.0,1.0))
                    z[rzcomp-1, rycomp-1, rxcomp-1] = np.conj(z[rz-1,ry-1,rx-1])
               
                    if((rz == rzcomp) and (ry == rycomp) and (rx == rxcomp)):
                        "sqrt(2) is required to rescale the RV"
                   
                        z[rz-1, ry-1, rx-1] = 0.5*(z[rz-1, ry-1, rx-1] + np.conj(z[rz-1, ry-1, rx-1]))*np.sqrt(2)
                   
                   
            
    
    coef[0,0] = 0
    Mat = np.multiply(coef,z)
    
    Y = int(Nx*Ny*Nz)*np.fft.ifftn(Mat)
    
    return np.real(Y)
'''

" NOT REQUIRED FOR 3D Gaussian Random Field Generation "
def ElistAssign(ELIST, RF, xi, yi, zi,  dx, dy, dz):
    " This code requires ELIST.npy and RF.npy files or Arrays"
    " The ELIST array has NxM no. of rows having element no. and integration point"
    "information"

    "RF array is an NzxNyxNx array, (X,Y, Z), having Random field"
    
    "THIS CODE assigns the 3D grid field (RF) to 1D form of ELIST"
    "The output is YFIELD"
    "xi,yi, zi are the co-ordinates of the corner of the domain"
    "dx, dy and dz are the element lengths along x, y and z direction respectively"
    
    " Note: use numpy.loadtxt('ELIST.txt', dtype = float) to load ELIST in python"
    
    YFIELD = np.zeros((np.size(ELIST,0),2))
    
    for i in range(0,np.size(ELIST,0)):
        
        YFIELD[i,0] = ELIST[i,0]
        
        x = ELIST[i,1]
        y = ELIST[i,2]
        z = ELIST[i,3]
        
        
        rx = int(round(((x-xi)/dx) + 0.5))
        ry = int(round(((y-yi)/dy) + 0.5))
        rz = int(round(((z-zi)/dz) + 0.5))
        
        rz=(rz-1)
        ry=(ry-1)
        rx=(rx-1)
        
        
        YFIELD[i,1] = RF[rz, ry, rx]
        
    return YFIELD
'''


"THE MAIN SCRIPT READS INPUT PARAMETERS FROM THE INPUT.txt file and creates MATLAB ARRAYS of random fields"
def main():


    #STEP-1: READ IN THE INPUT SPECIFICATIONS:
    inputfile = open('INPUTDATA.txt', 'r+')
    "Convert the input file to a list of strings"
    inputlist = list(inputfile)

    counter = 0
    for item  in inputlist:
        "Ignore the first line"
        if(counter > 0):
            inputstring = item
            "Remove all the whitespace"
            inputstring.strip()
            "Remove the newline character ~ \n"
            inputstring = inputstring.replace('\n','')
            "Partition the string about '=' "
            temp = inputstring.partition('=')
            
            "FILL THE INPUT DATA"

            " Here, m is the margin factor for the randon field domain. This is used to create an aperiodic random field"
            " Lx0, Ly0, Lz0 are the domain lengths along x,y,z direction in FEM model"
            " b is the correlation length for the isotropic exponential model"
            "Nx0, Ny0, Nz0 defines the number of elements along x, y, and z directions respectively in a Nx x Ny x Nz grid model"
            "Ns ~ No. of simulations"
            if(temp[0].strip()  == 'Lx'):
                Lx0 = float(temp[2])
            elif(temp[0].strip() == 'Ly'):
                Ly0 = float(temp[2])
            elif(temp[0].strip() == 'Lz'):
                Lz0 = float(temp[2])
            elif(temp[0].strip() == 'margin_parameter'):
                m = float(temp[2])
            elif(temp[0].strip() == 'correlation_length'):
                b = float(temp[2])
            elif(temp[0].strip() == 'Nx'):
                Nx0 = int(temp[2])
            elif(temp[0].strip() == 'Ny'):
                Ny0 = int(temp[2])
            elif(temp[0].strip() == 'Nz'):
                Nz0 = int(temp[2])
            elif(temp[0].strip() == 'Ns'):
                Ns = int(temp[2])
        counter = counter + 1
    
    "NOE defines the no. of elements in the model"
   
    NOE = Nx0*Ny0*Nz0

    "dx, dy, dz are grid units ~ i.e. the size of the rectangular mesh element"
    dx = Lx0/Nx0
    dy = Ly0/Ny0
    dz = Lz0/Nz0

    "Lx, Ly, Lz are the domain lengths along x,y,z direction in Randon Field model"
    Lx = 0.0
    Ly = 0.0
    Lz = 0.0

    "xi,yi are the co-ordinates of the left-bottom end of the domain"
    xi = 0.0
    yi = 0.0
    zi = 0.0

    "No. of elements to be added to create boundary margins"
    adddx = np.ceil(m*b/dx)
    adddy = np.ceil(m*b/dy)
    adddz = np.ceil(m*b/dz)

    Lx = Lx0 + 2*adddx*dx
    Ly = Ly0 + 2*adddy*dy
    Lz = Lz0 + 2*adddz*dz

    "Nx, Ny, Nz are the modified number of elements in the random field model"
    Nx = int(Nx0 + int(2*adddx))
    Ny = int(Ny0 + int(2*adddy))
    Nz = int(Nz0 + int(2*adddz))

    for i in range(0,Ns):
        
        "Generate the random field"
        RFfull = RFG3D(Lx, Ly, Lz, b, Nx, Ny, Nz)

        "Cut out the required portion - to make it aperiodic"
        RF = RF = RFfull[int(adddz):int(Nz0 + adddz), int(adddy):int(Ny0 + adddy), int(adddx):int(Nx0 + adddx)]

        RFGname = 'RFG' + str(i+1)
        sio.savemat(RFGname, mdict={RFGname: RF})

main()

